## Description
Fidibo test project to convert Wikipedia Farsi to Epub documents.


## To Run
I consider you already have node.js + npm installed on your machine.


## install modules
`npm i --save`


## Run 
`npm start`

### Info
http://localhost:3000/api/epub?url=[url of wikipedia page]
