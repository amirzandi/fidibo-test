import { Controller, Get, HttpException, Query, Res } from '@nestjs/common';
import { AppService } from './app.service';
import {Response} from "express";

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }



  @Get('/epub')
  async generateEpub(@Query() params: any, @Res() res: Response) { 
    if(params.url) { 
      const file = await this.appService.generateEpub(params.url);
      res.status(200).download(file);
    }
    else { 
      throw new HttpException('incomplete request', 400);
    }
  }
}
