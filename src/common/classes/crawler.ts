import * as https from "https";
import * as cheerio from "cheerio";


export class DataReader { 
    
    async read(url: string):Promise<any> { 
        return new Promise((resolve, reject) => { 
            try{
                https.get(url, (res)=> {
                    
                    let data = '';

                    res.on('data', (chunk) => {
                        data += chunk;
                    });

                    res.on('end',async () => {
                        
                        const $ = cheerio.load(data.toString());
                        
                        const title = $('[id=firstHeading]')['0'].children[0].data;
                        
                        const html = await this.generateHTML($, $('[class=mw-parser-output]')['0']);

                        

                        resolve({
                            title: title,
                            body: html
                        });
                    })
                });
            }
            catch(err) {
                reject(err);
            }
        })
    }


    private generateHTML($:any, body:any) { 
        let html = '';
        const pArr = [];
        for(let child of body.children) { 
            if(child.type == 'tag' && child.name=='p') { 
                html += ($(child).html()) + "<br/><br/>";
            }
        }
         
        return html;
    }
    
}