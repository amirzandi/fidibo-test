import { Injectable } from '@nestjs/common';
import { DataReader } from "./common/classes/crawler";
import * as epub from "epub-gen";

@Injectable()
export class AppService {
  getHello(): string {
    return 'Hello World!';
  }


  async generateEpub(url: string): Promise<any> {
    return new Promise(async (resolve, reject) => { 
      const body = await new DataReader().read(url);
      
      const option = { 
        title: body.title,
        author: 'amir zandi',
        output: './file.epub',
        content: [ 
          {
            title: body.title,
            data: body.body
          }
        ],
        css: `* { direction: rtl; text-align: right; }
        p { line-height: 1.5em; direction:rtl!important; text-align: right; }
        a { text-decoration: none; color: black; }
        `
      };
      new epub(option).promise.then((data) => {
        resolve('./file.epub');
      });

    })
    
  }
}
